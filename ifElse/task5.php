<?php
echo "Определить оценку студента";


function grade ($a) {
    if ($a >= 0 && $a < 20)
    return 'F';

    if ($a >= 20 && $a < 40)
    return 'E';

     if ($a >= 40 && $a < 60)
     return 'D';

     if ($a >= 60 && $a < 75)
    return 'C';

     if ($a >= 75 && $a < 90)
     return 'B';

     if ($a >= 90 && $a <= 100)
     return 'A';

     if ($a < 0 || $a > 100)
     return 'Такой оценки не существует';
 
}

echo "<br>". grade(10);
echo "<br>". grade(30);
echo "<br>". grade(50);
echo "<br>". grade(70);
echo "<br>". grade(80);
echo "<br>". grade(95);
echo "<br>". grade(105);