<?php
echo "Определить в какой координатной плоскости находится точка";

function koordinata($x, $y) {
    if ($y > 0 && $x != 0)
    {
        if ($x > 0) return 1;               
        return 2;
    }
    if ($y < 0 && $x != 0)
    {
        if ($x < 0) return 3;
        return 4;
    }
    if ( $x==0 || $y==0 )
    return 0;

}

echo "<br>". koordinata(1,1);
echo "<br>". koordinata(-1,-1);
echo "<br>". koordinata(-1,1);
echo "<br>". koordinata(1,-1);
echo "<br>". koordinata(0,1);
echo "<br>". koordinata(-1,0);