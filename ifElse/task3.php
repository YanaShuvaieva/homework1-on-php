<?php
echo "Найти суммы только положительных из трёх чисел";

function summa($a, $b, $c) {
$s = 0;

if ($a > 0) $s += $a;
if ($b > 0) $s += $b;
if ($c > 0) $s += $c;
   return $s;  

}

echo "<br>". summa(-2,3,5);
echo "<br>". summa(2,3,5);
echo "<br>". summa(2,-3,5);
echo "<br>". summa(2,3,-5);
echo "<br>". summa(-2,-3,-5);
echo "<br>". summa(2,-3,-5);