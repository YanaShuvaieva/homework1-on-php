<?php
echo "Найти индекс максимального элемента массива";

$arr = [6, 45, 23, 62, 7, 10, 9];
function maxindex($arr) {
    $maxIndex = 0;
    $max = $arr[0];
    for($i = 0; $i < 7; $i++) {

        if($arr[$i] > $max) {
        $max = $arr[$i];
        $maxIndex = $i;

        }   
    }
    return $maxIndex;
}

echo "<br>". maxindex($arr);